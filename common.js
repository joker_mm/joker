/**
 * 成功返回
 * @param {*} ctx 
 * @param {*} data 数据
 * @param {*} msg 描述
 */
 const success = async function(ctx,data=[],msg='操作成功'){
    ctx.response.body = {
        code: 200,
        data: data,
        msg: msg
    };
}

/**
 * 失败返回
 * @param {*} ctx 
 * @param {*} msg 描述
 * @param {*} code 状态码
 */
 const error = async function(ctx,msg='操作异常',code=-200){
    ctx.response.body = {
        code: code,
        msg: msg
    };
}

module.exports = {
    request_success:success,
    request_error:error
}