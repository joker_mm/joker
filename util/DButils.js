const knexConfig = require('../src/config/db.js');
let dbConfig = {};
if(knexConfig.hasOwnProperty(process.env.NODE_ENV)){
    dbConfig = knexConfig[process.env.NODE_ENV];
}else{
    dbConfig = knexConfig.default;
}
module.exports = {
    knex: require("knex")(dbConfig),
};
