const {knex} = require("../../util/DButils");
/**
 * 查询
 * @param {*} ctx 
 * @param {*} req 
 * @return {object}
 */
const list = async function(ctx,req){
    let list = await knex.from('user').select();
    return {
        data:list,
        msg:'查询成功'
    }
}

module.exports = {
    list:list
}