module.exports = {
    default:{
        "client": "mysql",
        "connection": {
            "host": "127.0.0.1",
            "user": "root",
            "password": "rootroot",
            "database": "demo",
            "timezone": "UTC",
            "dateStrings": true
        },
        "fetchAsString": ["number"],
        "acquireConnectionTimeout": 10000
    },
    produce:{
        "client": "mysql",
        "connection": {
            "host": "127.0.0.1",
            "user": "root",
            "password": "rootroot",
            "database": "demo",
            "timezone": "UTC",
            "dateStrings": true
        },
        "fetchAsString": ["number"],
        "acquireConnectionTimeout": 10000
    },
    dev:{
        "client": "mysql",
        "connection": {
            "host": "127.0.0.1",
            "user": "root",
            "password": "rootroot",
            "database": "demo",
            "timezone": "UTC",
            "dateStrings": true
        },
        "fetchAsString": ["number"],
        // "pool": {
        //     "min": 1,
        //     "max": 10
        // },
        "acquireConnectionTimeout": 10000
    }
}